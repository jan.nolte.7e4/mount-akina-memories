package cat.itb.mountakinamemories


data class Card (var imageFlipped:Int, var image:Int, var viewId:Int,var flipped: Boolean=true, var blocked:Boolean=false)