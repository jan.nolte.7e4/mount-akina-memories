package cat.itb.mountakinamemories

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModelProvider

class game_screen_easy : AppCompatActivity() {
    lateinit var layout:ConstraintLayout
    lateinit var back_button: Button
    lateinit var viewmodel : GameViewModel
    var imageViewList = mutableListOf<ImageView>()
    lateinit var mediaPlayer: MediaPlayer
    var parelles:Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MountAkinaMemories)
        super.onCreate(savedInstanceState)
        viewmodel = ViewModelProvider(this).get(GameViewModel::class.java)
        mediaPlayer = MediaPlayer.create(this, R.raw.gamingsong)
        mediaPlayer.setLooping(true)
        playSound(mediaPlayer)
        val bundle:Bundle? = intent.extras
        var startparelles = bundle?.getInt("parelles")
        if(startparelles != null){
            parelles = startparelles
            if(!viewmodel.gameRunning){
                viewmodel.setGameScore(0)
                viewmodel.startGame()
                viewmodel.lastpickID =0
            }
        }
        when(parelles){
            2 -> setContentView(R.layout.activity_game_screen2)
            3 -> setContentView(R.layout.activity_game_screen3)
            4 -> setContentView(R.layout.activity_game_screen4)
            6 -> setContentView(R.layout.activity_game_screen6)
            else->{
                parelles = 3
                setContentView(R.layout.activity_game_screen3)
            }
        }

        layout = findViewById(R.id.easy_game)
        viewmodel.shuffleParelles(parelles)
        var parella=0
        for (i in (0..layout.childCount)) {
            var view = layout.getChildAt(i)
            if (view is ImageView) {
                imageViewList.add(view)
                if(viewmodel.arrayCards.size<=(parelles*2)-1) {
                    viewmodel.addCard(
                        Card(
                            R.drawable.backimagecards,
                            viewmodel.randomImage(parella),
                            view.id
                        )
                    )
                    parella++
                }else{
                    view.setImageResource(viewmodel.cardState(view.id))
                }
            }
        }
        val intentResult = Intent(this, ResultActivity::class.java)
        if(viewmodel.winGame()){
            startActivity(intentResult)
        }
        viewmodel.printCards()
        imageViewList.forEach{view->
            view.setOnClickListener{
                view.setImageResource(viewmodel.showCard(view.id))
                val hideCards = object: CountDownTimer(500, 500) {
                    override fun onTick(millisUntilFinished: Long) {}

                    override fun onFinish() {
                        if(viewmodel.lastpickID == 0){
                            viewmodel.lastpickID = view.id
                        }else if(viewmodel.lastpickID != view.id){
                            if(!viewmodel.compareCards(viewmodel.lastpickID, view.id)){
                                view.setImageResource(viewmodel.hideCard(view.id))
                                findViewById<ImageView>(viewmodel.lastpickID).setImageResource(viewmodel.hideCard(viewmodel.lastpickID))
                            }
                            viewmodel.lastpickID = 0
                            if(viewmodel.winGame()){
                                intentResult.putExtra("score", viewmodel.score)
                                viewmodel.stopGame()
                                startActivity(intentResult)
                            }
                        }

                    }
                }
                hideCards.start()
                viewmodel.printCards()
            }
        }
        back_button = findViewById(R.id.back_button)
        back_button.setOnClickListener{
            val alertDialog= AlertDialog.Builder(this)
            alertDialog.setMessage("HELP")
                .setMessage("Score: "+viewmodel.score)
                .setNegativeButton("Return to game",
                    DialogInterface.OnClickListener { dialog, id ->
                        // Torna al menu!
                    })
            val missage: Dialog = alertDialog.create()
            missage.setCanceledOnTouchOutside(false)
            missage.show()
        }
    }
    override fun onResume() {
        if(!mediaPlayer.isPlaying())mediaPlayer.start()
        super.onResume()
    }
    override fun onDestroy() {
        mediaPlayer.stop()
        super.onDestroy()
    }
    override fun onPause() {
        mediaPlayer.pause()
        super.onPause()
    }
    fun playSound(mp: MediaPlayer){
        mp.seekTo(0)
        mp.start()
    }
}