package cat.itb.mountakinamemories

import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Configuration
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider

class MainActivity : AppCompatActivity() {
    lateinit var playButton: Button
    lateinit var helpButton: Button
    lateinit var mediaPlayer: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MountAkinaMemories)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if(isDarkModeOn()){
            mediaPlayer = MediaPlayer.create(this, R.raw.nightsong)
        }else{
            mediaPlayer = MediaPlayer.create(this, R.raw.daysong)
        }
        playSound(mediaPlayer)
        var parelles = 0
        val spinner: Spinner = findViewById(R.id.difficulty_settings)
        val listDifficulties = resources.getStringArray(R.array.diffult_levels)
        spinner.adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, listDifficulties)
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when (listDifficulties[p2]) {
                    "Akina" -> parelles = 2
                    "Myogi" -> parelles = 3
                    "Usui" -> parelles = 4
                    "Maze" -> parelles = 6
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                parelles = 2
            }

        }
        playButton = findViewById(R.id.screen1_play_button)
        playButton.setOnClickListener {
            val intent = Intent(this, game_screen_easy::class.java)
            intent.putExtra("parelles", parelles)
            startActivity(intent)
        }
        helpButton = findViewById(R.id.screen1_help_button)
        helpButton.setOnClickListener {
            val alertDialog = AlertDialog.Builder(this)
            alertDialog.setMessage("Pause")
                .setMessage(resources.getString(R.string.help_message))
                .setNegativeButton("Return to menu",
                    DialogInterface.OnClickListener { dialog, id ->
                        // Torna a la partida!
                    })
            val missage: Dialog = alertDialog.create()
            missage.setCanceledOnTouchOutside(false)
            missage.show()
        }
    }

    override fun onResume() {
        if(!mediaPlayer.isPlaying())mediaPlayer.start()
        super.onResume()
    }
    override fun onDestroy() {
        mediaPlayer.stop()
        super.onDestroy()
    }
    override fun onPause() {
        mediaPlayer.pause()
        super.onPause()
    }
    private fun isDarkModeOn(): Boolean {
        val currentNightMode = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        return currentNightMode == Configuration.UI_MODE_NIGHT_YES
    }
    fun playSound(mp:MediaPlayer){
        mp.seekTo(0)
        mp.start()
    }

}