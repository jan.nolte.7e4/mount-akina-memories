package cat.itb.mountakinamemories

import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.ViewModel

class GameViewModel : ViewModel() {
    var arrayCards= mutableListOf<Card>()
    var arrayImages : IntArray = intArrayOf(
        R.drawable.iketani,
        R.drawable.takumi,
        R.drawable.mako,
        R.drawable.takaheshi,
        R.drawable.keisuke,
        R.drawable.reysuke,
        R.drawable.rinhojo
    )
    var llistaImatges = mutableListOf<Int>()
    var score : Int =0
    var numParelles:Int =3
    var lastpickID:Int = 0
    var gameRunning:Boolean = false
    init{
        arrayImages.shuffle()
    }
    fun addCard (c:Card){
        arrayCards.add(c)
    }
    fun searchCard(id:Int):Card{
        arrayCards.forEach{card->
            if(card.viewId==id){
                return card
            }
        }
        return arrayCards[0]
    }
    fun showCard(id:Int):Int{
        var c = searchCard(id)
        c.flipped  = false
        return c.image
    }
    fun hideCard(id:Int):Int{
        var c = searchCard(id)
        if(!c.blocked){
            c.flipped = true
            return c.imageFlipped
        }else{
            return c.image
        }
    }
    fun flipCard(id:Int):Int{
        var c = searchCard(id)
        if(c.flipped){
            c.flipped = false
            return c.image
        }else{
            c.flipped = true
            return c.imageFlipped
        }
    }
    fun randomImage(parelles:Int):Int{
        return llistaImatges[parelles]
    }
    fun cardState(id:Int):Int{
        var c = searchCard(id)
        if(c.flipped){
            return c.imageFlipped
        }else{
            return c.image
        }
    }
    fun compareCards(lc:Int, ac:Int):Boolean{
        if(searchCard(lc).image == searchCard(ac).image){
            searchCard(lc).blocked = true
            searchCard(ac).blocked =true
            addGameScore(1000)
            return true
        }
        addGameScore(-100)
        return false
    }
    fun shuffleParelles(parelles: Int){
        numParelles = parelles
        //setGameScore(0)
        if(llistaImatges.size<=parelles*2-1){
            var sel =0
            for(i in(0..(parelles*2)-1)){
                llistaImatges.add(i, arrayImages[sel])
                if(i%2!=0 && i!=0){
                    sel++
                }
            }
            for (i in (0..2)){
                llistaImatges.shuffle()
            }
        }
    }
    fun winGame():Boolean{
        arrayCards.forEach{
            c-> if(!c.blocked) return false
        }
        return true
    }
    fun printCards(){
        println("Start:")
        arrayCards.forEach { c->println(c.viewId.toString()+":\tBlocked "+c.blocked+"\tflipped "+c.flipped)}
    }
    fun addGameScore(value:Int){
        score+=value
    }
    fun setGameScore(value:Int){
        score = value
    }
    fun startGame(){
        gameRunning = true
    }
    fun stopGame(){
        gameRunning = false
    }
    fun isGameRunning():Boolean{
        return gameRunning
    }
}