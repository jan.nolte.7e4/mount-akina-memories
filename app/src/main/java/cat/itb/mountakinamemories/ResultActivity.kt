package cat.itb.mountakinamemories

import android.content.Intent
import android.media.MediaPlayer
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import org.w3c.dom.Text

class ResultActivity : AppCompatActivity() {
    lateinit var textScore: TextView
    lateinit var returnButton:Button
    lateinit var mediaPlayer: MediaPlayer
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_MountAkinaMemories)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.result)
        mediaPlayer = MediaPlayer.create(this, R.raw.winsong)
        playSound(mediaPlayer)
        val bundle:Bundle? = intent.extras
        var score = bundle?.getInt("score")
        textScore = findViewById(R.id.scorevalue)
        textScore.setText(score.toString())
        returnButton = findViewById(R.id.back_button)
        returnButton.setOnClickListener{
            val startIntent = Intent(this, MainActivity::class.java)
            startActivity(startIntent)
        }
    }
    override fun onResume() {
        if(!mediaPlayer.isPlaying())mediaPlayer.start()
        super.onResume()
    }
    override fun onDestroy() {
        mediaPlayer.stop()
        super.onDestroy()
    }
    override fun onPause() {
        mediaPlayer.pause()
        super.onPause()
    }
    fun playSound(mp: MediaPlayer){
        mp.seekTo(0)
        mp.start()
    }
}